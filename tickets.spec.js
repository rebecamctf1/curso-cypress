/// <reference types="cypress" />
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"))
    //beforeEach(()) serve para não repetir a linha de código
    //que seria entrar na URL
    //primeiro teste: preencher todos os campos de texto no site
    //.only serve para executar apenas essa linha de teste
    //procurar a biblioteca MOCHA
    it("fills all the text input fields", () => {
        const firstName = "Rebeca";
        const lastName = "Figueiredo";
        
        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get("#email").type("rebecamctf@outlook.com");
        cy.get("#requests").type("vegan");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });
    //para acessar drop-down
    it("select two tickets", () => {
        cy.get('#ticket-quantity').select("2");
    });
    //para selecionar radio buttons
    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });
    //interagir com os checkboxes
    it("selects 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("selects 'friend', and 'publication', then untrack 'publication'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });
// verificando o estado da aplicação - Assertions
    it("alerts on invalid email", () => {
        //cy.get("#email").type("talkingabouttesting-gmail.com");
        //criando um alias ("@nome_do_campo") para o id email
        cy.get("#email")
          .as("email")
          .type("talkingabouttesting-gmail.com"); 

        //cy.get("#email.invalid").should("exist");
        cy.get("#email.invalid")
          //.as("invalidEmail") - nesse caso não pode usar mais de 1 alias
          .should("exist");

        cy.get("@email")
          .clear() //limpa o campo do email
          .type("talkingabouttesting@gmail.com"); 

        cy.get("#email.invalid").should("not.exist");
    });

// Criando testes End-To-End

    it.only("fills and reset the form", () => {
        const firstName = "Rebeca";
        const lastName = "Figueiredo";
        const fullName = `${firstName} ${lastName}`;

//preenche todos os campos de string
        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get("#email").type("rebecamctf@outlook.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Sorvete");
//a quantidade de tickets pode ser substituida por uma variavel que recebe o valor
        cy.get(".agreement p").should(
          'contain',
          `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

//verifica que o botao nao esta desabilitado
        cy.get("button[type='submit']")
          .as("submitbutton")
          .should("not.be.disabled");

//verifica que o botao esta desabilitado
        cy.get("button[type='reset']").click();

        cy.get("@submitbutton").should("be.disabled");
        
    });
});